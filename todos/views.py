from django.shortcuts import render

from todos.models import TodoItem, TodoList


# Create your views here.
def todo_list_list(request):
    lists = TodoList.objects.all()
    context = {"lists": lists}
    return render(request, "todos_lists/list.html/", context)


def todo_list_detail(request, id):
    list = TodoList.objects.get(id=id)
    context = {"list": list}
    return render(request, "todos_lists/detail.html", context)
